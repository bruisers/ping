﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.NetworkInformation;

namespace ping
{
    class Program
    {
        static void Main(string[] args)
        {
            Ping p = new Ping();
            PingOptions op = new PingOptions();
            op.DontFragment = true;
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            int timeout = 120;
            PingReply reply = p.Send("192.168.78.53", timeout, buffer, op); ///192.168.78.51 is local address.
            if (reply.Status == IPStatus.Success)
            {
                Console.WriteLine("Success");
                Console.WriteLine("Address: {0}", reply.Address.ToString());
                Console.WriteLine("RoundTrip time: {0}", reply.RoundtripTime);
                Console.WriteLine("Time to live: {0}", reply.Options.Ttl);
                Console.WriteLine("Don't fragment: {0}", reply.Options.DontFragment);
                Console.WriteLine("Buffer size: {0}", reply.Buffer.Length);
            }
            else
            {
                Console.WriteLine("Sorry");
            }
            Console.ReadLine();
        }
    }
}
